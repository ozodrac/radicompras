-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 07-Nov-2015 às 04:28
-- Versão do servidor: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `radicompras`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `anuncios`
--

CREATE TABLE IF NOT EXISTS `anuncios` (
  `idanuncio` int(10) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `tamanho` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `preco` decimal(10,0) NOT NULL,
  `uso` enum('U','N') COLLATE latin1_general_ci NOT NULL COMMENT 'U=usado ; N=novo',
  `estado` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `cidade` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `bairro` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `telefone` varchar(25) COLLATE latin1_general_ci NOT NULL,
  `detalhes` text COLLATE latin1_general_ci,
  `data_cad` datetime NOT NULL,
  `ativo` enum('S','N') COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`idanuncio`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `anuncio_categorias`
--

CREATE TABLE IF NOT EXISTS `anuncio_categorias` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `idcategoria` int(10) NOT NULL,
  `idanuncio` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `anuncio_fotos`
--

CREATE TABLE IF NOT EXISTS `anuncio_fotos` (
  `idfotoanuncio` int(10) NOT NULL AUTO_INCREMENT,
  `idanuncio` int(11) NOT NULL,
  `foto` varchar(55) COLLATE latin1_general_ci NOT NULL COMMENT 'nome da foto',
  `ativo` enum('S','N') COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`idfotoanuncio`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `anuncio_usuario`
--

CREATE TABLE IF NOT EXISTS `anuncio_usuario` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `idusuario` int(10) NOT NULL,
  `idanuncio` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `categorias`
--

CREATE TABLE IF NOT EXISTS `categorias` (
  `idcategoria` int(10) NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `descricao` varchar(200) COLLATE latin1_general_ci DEFAULT NULL,
  `ativo` enum('S','N') COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`idcategoria`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `email` varchar(55) COLLATE latin1_general_ci NOT NULL,
  `senha` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `cpf` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `site` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  `sobre` text COLLATE latin1_general_ci,
  `facebook` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  `data_cad` datetime NOT NULL,
  `ultimo_login` datetime DEFAULT NULL,
  `ativo` enum('S','N') COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`,`cpf`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
